import websockets
import asyncio
import json

async def listen():
    url = 'ws://localhost:9895'

    async with websockets.connect(url) as ws:
        await ws.send('{"action":"file.load", "data":"file_path"}')
        while True:
            msg = await ws.recv()
            print(type(msg))
            print(msg)
            data = json.loads(msg)
            
            print("Action: {0}".format(data['action']))
            print("File: {0}".format(data['data']))


asyncio.get_event_loop().run_until_complete(listen())