import websockets
import asyncio

PORT=9895

print("Sever Starting on port {0}".format(PORT))

async def echo(web_socket, path):
    print("Client connected")

    async for message in web_socket:
        print("Received: {0}".format(message))
        await web_socket.send(message)

start_server = websockets.serve(echo, 'localhost', PORT)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()