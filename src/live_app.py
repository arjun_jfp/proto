#!/usr/bin/env python

import argparse
from rez.utils.formatting import get_epoch_time_from_str
from nirvana_core.scm import profapp

def main(profile, command, patch_list, time, dry_run, test_mode, no_local):
    timestamp = None
    if time:
        timestamp = get_epoch_time_from_str(time)
    app = profapp.ProfApp(profile, 
                        command,
                        patch_packages=patch_list,
                        timestamp=timestamp,
                        test_mode=test_mode,
                        no_local=no_local)
    app.launch_app(dry_run=dry_run)

if __name__=='__main__':
    parser = argparse.ArgumentParser(description ='Config resolver')
    parser.add_argument('profile', help ='Name of the app profile like maya_2020')
    parser.add_argument('command', help ='Command to be executed within profile')
    parser.add_argument('--patch', nargs='+', help ='Packages to patched', default=None)
    parser.add_argument('--dry-run', action='store_true', help= "Dosen't execute command")
    parser.add_argument('--no-local', action='store_true', help= "Dosen't load packages from local")
    parser.add_argument('--test', action='store_true', help ='Includes path for test packages')
    parser.add_argument('--time', help ='Time to resolve packages --time=-1d', default=None)

    args = parser.parse_args()
    main(args.profile, 
        args.command,
        args.patch,
        args.time,
        args.dry_run,
        args.test,
        args.no_local)