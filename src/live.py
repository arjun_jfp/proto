import datetime
from rez import resolved_context
from rez.config import config as rezconfig
from nirvana_core import profig
from nirvana_core.nio import logger

def get_epoch_time(year, month, day, hour, minute):
        calculated_time = datetime.datetime(int(year), 
                                            int(month), 
                                            int(day),
                                            int(hour),
                                            int(minute),
                                            )
        return calculated_time.timestamp()

class ProfApp(object):
    def __init__(self,
                profile,
                command,
                include_filters=None,
                timestamp=None,
                patch_packages=None,
                no_local=False,
                test_mode=False):
        self._profile = profile
        self._command = command
        self._include_filters = include_filters
        self._timestamp = timestamp
        self._patch_packages = patch_packages
        self._config = None
        self._no_local = no_local
        self._test_mode = test_mode
        self._set_config()

    def __repr__(self):
        return "{0}: {1}".format(self.profile, self.command)

    def _set_config(self):
        self._config = profig.get_config('nirvana_core', 'live_resolve')

    def get_profile_packages(self):
        package_list = getattr(self.config, self.profile).packages
        return package_list

    def is_valid_day(self, day):
        '''
        Validate if the given day is a open day in the 
        config.
        '''
        if day in self.config.open_days:
            return True
        else:
            False

    def calculate_timestamp(self):
        '''
        If timestamp is provides as the argument, that take priority
        second priority is for the timestamp locked. If none of the 
        above is provided timestamp is calculated from the rules in
        configuration
        '''
        # If user didn't provide a timestamp, look if the config has
        # a timestamp lock. If user has provided the below code don't
        # execute timestamp is directly applied to the resolve
        if not self.timestamp:
            if self.config.lock:
                self.timestamp = self.config.lock
            else:
                today = datetime.datetime.now()
                day = today.strftime('%a')
                # if today is a valid day, find calculate the epoch time
                if self.is_valid_day(day):
                    self.timestamp = get_epoch_time(int(today.year), 
                                                        int(today.month), 
                                                        int(today.day),
                                                        int(self.config.cut_off[0]),
                                                        int(self.config.cut_off[1]),
                                                        )
                else:
                    # Looping in reverse to find the previous valid day
                    # range(7) is the number of days in a week
                    for i in range(7):
                        previous_day = today - datetime.timedelta(days=i)
                        day = previous_day.strftime('%a')
                        if self.is_valid_day(day):
                            self.timestamp = get_epoch_time(int(previous_day.year), 
                                                        int(previous_day.month), 
                                                        int(previous_day.day),
                                                        int(self.config.cut_off[0]),
                                                        int(self.config.cut_off[1]),
                                                        )
                            break

    def get_packages_path(self):
        packages_path = rezconfig.packages_path
        if self.no_local:
            packages_path = rezconfig.nonlocal_packages_path
        
        if self.test_mode:
            packages_path.insert(0, self.config.test_packages)
        return packages_path

    def resolve(self):
        package_list = self.get_profile_packages()
        self.calculate_timestamp()
        data = {
            'package_requests': package_list,
            'timestamp': self.timestamp,
            'package_paths': self.get_packages_path()
        }
        
        context = resolved_context.ResolvedContext(**data)

        if self.patch_packages:  
            patched_context = context.get_patched_request(self.patch_packages)
            data['package_requests'] = patched_context
            data.pop('timestamp', None)
            context = resolved_context.ResolvedContext(**data)

        return context

    def launch_app(self, dry_run=False):
        context = self.resolve()
        context.print_info()
        if self.patch_packages:
            logger.warn("When patched ignores timestamp")

        if not dry_run:
            context.execute_shell(command = self.command,
                                block=True
                                )

    @property
    def config(self):
        return self._config

    @property
    def profile(self):
        return self._profile

    @property
    def command(self):
        return self._command

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value):
        self._timestamp = value

    @property
    def patch_packages(self):
        return self._patch_packages

    @property
    def no_local(self):
        return self._no_local

    @property
    def test_mode(self):
        return self._test_mode