class Profig:
    def __init__(self, **config_dict):
        for k,v in config_dict.items():
            if isinstance(v,dict):
                self.__dict__[k] = Profig(**v)
            else:
                self.__dict__[k] = v
    
    def to_json(self):
        return json.loads(json.dumps(self, default=lambda o: o.__dict__))