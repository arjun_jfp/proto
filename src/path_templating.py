import math
import re
from pprint import pprint

def get_tokens(template_name):
    pass

def encode_path(template, data):
    '''
    Encodes path given template name and mapping dict
    '''
    return template.format(**data)


def decode_path(file_path, template):
    '''
    We assume all of the arguments in format string are named keyword arguments (i.e. no {} or
    {:0.2f}). We also assume that all chars are allowed in each keyword argument, so separators
    need to be present which aren't present in the keyword arguments (i.e. '{one}{two}' won't work
    reliably as a format string but '{one}-{two}' will if the hyphen isn't used in {one} or {two}).

    We raise if the format string does not match s
    '''
    tokens = re.split(r'\{(.*?)\}', template)
    keywords = tokens[1::2]

    tokens[1::2] = map(u'(?P<{}>.*)'.format, keywords)
    tokens[0::2] = map(re.escape, tokens[0::2])
    pattern = ''.join(tokens)
    matches = re.match(pattern, file_path)

    if not matches:
        raise Exception("Format string did not match")

    return {x: matches.group(x) for x in keywords}


template_data = {
    'drive': 'N:',
    'project':'sample',
    'asset_type':'prop',
    'asset_name':'keyboard',
    'step':'MDL',
    'user_name':'arjun',
    'task':'pipeline'
}

print('-------------------- Encoding --------------------')
temp_name = '{drive}/{project}/publish/{asset_type}/{asset_name}/{step}/{user_name}_{task}'
final_file_path = 'N:/sample/publish/prop/keyboard/MDL/arjun_pipeline'
print('Template Name:\t\t{0}'.format(temp_name))
print('Data:\t\t\t{0}'.format(template_data))
print('Function:\t\tencode_path(template_name, data)')
print("Result:\t\t\t{0}".format(encode_path(temp_name, template_data)))

print('\n')
print('-------------------- Decoding --------------------')
print('Template Name:\t\t{0}'.format(temp_name))
print('Input Path:\t\t{0}'.format(final_file_path))
print('Function:\t\tdecode_path(file_path, template_name)')
print("Result:\t\t\t{0}".format(decode_path(final_file_path, temp_name)))